from unittest import main, TestCase

from Diplomacy import diplomacy_solve

class TestDiplomacy(TestCase):
	
	def test_solve_1(self):
		x = diplomacy_solve("A Madrid Hold")
		self.assertEqual(x,"A Madrid\n")

	def test_solve_2(self):
		x = diplomacy_solve("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
		self.assertEqual(x,"A [dead]\nB Madrid\nC London\n")

	def test_solve_3(self):
		x = diplomacy_solve("A Madrid Hold\nB Barcelona Hold\nC London Hold")
		self.assertEqual(x,"A Madrid\nB Barcelona\nC London\n")

	def test_solve_4(self):
		x = diplomacy_solve("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
		self.assertEqual(x,"A [dead]\nB [dead]\nC [dead]\n")

if __name__ == "__main__":
    main()